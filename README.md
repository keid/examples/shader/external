# Keid - External shaders

Use `stack test` first to generate and compile initial shader code.

Use `stack run` to launch the example.

Run `stack test --file-watch --fast` in background and change the code in [Render.Example.Code](lib/Render/Example/Code.hs).
