module Render.Example.Pipeline
  ( Pipeline
  , allocate
  , allocateBlend

  , Config
  , config
  , configBlend

  , stageCode
  ) where

import RIO

import Control.Monad.Trans.Resource (MonadResource, ResourceT)
import Data.Tagged (Tagged(..))
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Types (DsLayoutBindings, HasRenderPass(..), MonadVulkan)
import Render.Code (compileVert, compileFrag)
import Render.DescSets.Set0 (Scene)
import Resource.Region qualified as Region
import Vulkan.Core10 qualified as Vk

import Render.Example.Code qualified as Code
import Render.Example.Model qualified as Model

type Pipeline = Graphics.Pipeline '[Scene] Model.Vertex Model.Attrs
type Config = Graphics.Configure Pipeline
type instance Graphics.Specialization Pipeline = ()

allocate
  :: ( MonadVulkan env m
     , MonadResource m
     , HasRenderPass renderpass
     )
  => Vk.SampleCountFlagBits
  -> Tagged Scene DsLayoutBindings
  -> renderpass
  -> ResourceT m Pipeline
allocate multisample tset0 =
  Region.local .
    Graphics.allocate
      Nothing
      multisample
      (config tset0)

allocateBlend
  :: ( MonadVulkan env m
     , MonadResource m
     , HasRenderPass renderpass
     )
  => Vk.SampleCountFlagBits
  -> Tagged Scene DsLayoutBindings
  -> renderpass
  -> ResourceT m Pipeline
allocateBlend multisample tset0 =
  Region.local . Graphics.allocate
    Nothing
    multisample
    (configBlend tset0)

config :: Tagged Scene DsLayoutBindings -> Config
config (Tagged set0) = Graphics.baseConfig
  { Graphics.cDescLayouts  = Tagged @'[Scene] [set0]
  , Graphics.cStages       = Graphics.basicStages vertSpirv fragSpirv
  , Graphics.cVertexInput  = Graphics.vertexInput @Pipeline
  }

configBlend :: Tagged Scene DsLayoutBindings -> Config
configBlend tset0 = (config tset0)
  { Graphics.cBlend      = True
  , Graphics.cDepthWrite = False
  }

stageCode :: Graphics.StageCode
stageCode = Graphics.basicStages Code.vert Code.frag

vertSpirv :: ByteString
vertSpirv = $(compileVert Code.vert)

fragSpirv :: ByteString
fragSpirv = $(compileFrag Code.frag)
