module Stage.Main.Event.Key
  ( callback
  , keyHandler
  ) where

import RIO

import UnliftIO.Resource (ReleaseKey)

import Engine.Events.Sink (MonadSink, Sink(..))
import Engine.Types (StageRIO)
import Engine.Window.Key (Key(..), KeyState(..))
import Engine.Window.Key qualified as Key
-- import Render.ImGui qualified as ImGui

import Stage.Main.Event.Type (Event)
import Stage.Main.Event.Type qualified as Event
import Stage.Main.Types (RunState(..))

callback :: Sink Event RunState -> StageRIO RunState ReleaseKey
callback = Key.callback . keyHandler

keyHandler :: MonadSink RunState m => Sink Event RunState -> Key.Callback m
keyHandler (Sink signal) keyCode keyEvent@(_mods, state, key) = do -- ImGui.capturingKeyboard do
  logInfo $ "Key event (" <> display keyCode <> "): " <> displayShow keyEvent
  case key of
    Key'Space ->
      signal Event.DoNothing
    -- Key'F2 | pressed ->
    --   signal Event.ToggleFullscreen
    Key'Escape ->
      signal Event.Stop
    _ ->
      pure ()
  where
    _pressed = state == KeyState'Pressed
