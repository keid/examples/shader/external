module Stage.Main.Event.Type
  ( Event(..)
  ) where

import RIO

data Event
  = DoNothing
  -- | ToggleFullscreen
  | Stop
  deriving (Eq, Show, Generic)
