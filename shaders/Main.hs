module Main where

import RIO

import Engine.SpirV.Compile qualified as Compile
import Render.Basic qualified as Basic
import RIO.FilePath ((</>))

import Render.Example.Pipeline qualified as Example

main :: IO ()
main = runSimpleApp do
  let out = "data" </> "shaders"
  Compile.glslStages (Just out) "example" Example.stageCode

  Compile.glslPipelines (Just Basic.shaderDir) Basic.stageSources
